package com.example.lab3.data;

import com.example.lab3.data.model.LoggedInUser;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "dj29649@zut.edu.pl:password:Jakub Dziedzic"
    };

    public Result<LoggedInUser> login(String username, String password) {
        try {
            for (String credential : DUMMY_CREDENTIALS) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(username)) {
                    if (pieces[1].equals(password)) {
                        LoggedInUser fakeUser =
                                new LoggedInUser(
                                        java.util.UUID.randomUUID().toString(),
                                        pieces[2]);
                        return new Result.Success<>(fakeUser);
                    }
                }
            }
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
        return new Result.Error(new IOException("Error logging in"));
    }

    public void logout() {
        // TODO: revoke authentication
    }
}