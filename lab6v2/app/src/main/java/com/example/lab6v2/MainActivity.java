package com.example.lab6v2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.lab6.R;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
    }

    public void gameList(View v)
    {
        Intent intencja = new Intent(getApplicationContext(), GamesList.class);
        intencja.putExtra("gra", v.getId());
        startActivity(intencja);
    }

}